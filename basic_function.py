import os
import sys
import time
import shutil
import random
import numpy as np
import torchnet as tnt

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.utils as vutils
import torchvision.models as models
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from torch.utils import data
from tensorboardX import SummaryWriter




def ColorMapping(seg):
    colormap = torch.Tensor([[0,0,0], [128,0,0], [0,128,0], [128,128,0], [0,0,128], [128,0,128],
                              [0,128,128], [128,128,128], [64,0,0], [192,0,0], [64,128,0], [192,128,0],
                              [64,0,128], [192,0,128], [64,128,128], [192,128,128], [0, 64,0], [128, 64, 0],
                              [0,192,0], [128,192,0], [0,64,128]])/255.0
    seg = torch.matmul(seg.transpose(1,2).transpose(2,3), colormap).transpose(3,2).transpose(2,1)
    return seg


# label = 255 is ambiguious label, and only some gts have this label.
class SegLoss(nn.Module):
    def __init__(self, ignore_label=255, mode=1):
        super(SegLoss, self).__init__()
        if mode==1:
            self.obj = torch.nn.CrossEntropyLoss(ignore_index=ignore_label)
        else:
            self.obj = torch.nn.NLLLoss2d(ignore_index=ignore_label)
    def __call__(self, pred, label):        
        loss = self.obj(pred, label)
        return loss
    
               
def get_1x_lr_params_NOscale(model):
    """
    This generator returns all the parameters of the net except for 
    the last classification layer. Note that for each batchnorm layer, 
    requires_grad is set to False in deeplab_resnet.py, therefore this function does not return 
    any batchnorm parameter
    """
    b = []

    b.append(model.conv1)
    b.append(model.bn1)
    b.append(model.layer1)
    b.append(model.layer2)
    b.append(model.layer3)
    b.append(model.layer4)

    for i in range(len(b)):
        for j in b[i].modules():
            jj = 0
            for k in j.parameters():
                jj+=1
                if k.requires_grad:
                    yield k
         
                    
                    
def get_10x_lr_params(model):
    """
    This generator returns all the parameters for the last layer of the net,
    which does the classification of pixel into classes
    """
    b = []
    b.append(model.layer5.parameters())
    b.append(model.layer6.parameters())
    b.append(model.layer7.parameters())
    
    for j in range(len(b)):
        for i in b[j]:
            yield i                    
                    
                    
                    
def adjust_learning_rate(optimizer, basic_lr, i_iter, max_iter):
    """Sets the learning rate to the initial LR divided by 5 at 60th, 120th and 160th epochs"""
    lr = lr_poly(basic_lr, i_iter, max_iter, 0.9)
    optimizer.param_groups[0]['lr'] = lr * 10
    #optimizer.param_groups[1]['lr'] = lr * 10
    return lr
    
   
    
def lr_poly(base_lr, iter, max_iter, power):
    return base_lr*((1.0-float(iter)/max_iter)**(power))
                    
    
    
def param_restore(model, param_dict):
    new_params = model.state_dict().copy()
    for i in param_dict:
        #debug()
        i_parts = i.split('.')
        if not i_parts[0]=='fc':
            new_params[i] = param_dict[i]
    model.load_state_dict(new_params)
    return model



def BGR2RGB(img):
    out = torch.zeros(img.size())
    out[:,0,:,:] = img[:,2,:,:]
    out[:,1,:,:] = img[:,1,:,:]
    out[:,2,:,:] = img[:,0,:,:]
    return out



def map_decode(seg, num_label=21):
    seg[seg==255]=0           #######################
    labels = range(num_label)
    batch_size = seg.size(0)
    out = torch.zeros(seg.size()).repeat(1,num_label,1,1)
    for i, label in enumerate(labels):
        #out_slice = out[:,i:i+1,:,:]1
        out[:,i:i+1,:,:] = seg==label
    return out

    


class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


        
def save_checkpoint(state, is_best, tag='train', check_dir='./'):
    path = os.path.join(check_dir, 'checkpoint.pth.tar')
    torch.save(state, path)
    if is_best:
        shutil.copyfile(path, os.path.join(check_dir, '{0}_model_best.pth.tar'.format(tag)))        
        

def BatchInverse(tensor):
    batch_size = tensor.size()[0]
    tensor_inverse = []
    for i in range(batch_size):
        tensor_inverse += [torch.inverse(tensor[i]).unsqueeze(0)]
    return torch.cat(tensor_inverse, 0)
