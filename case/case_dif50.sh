#/bin/bash
python ../main.py 0,1,2,3 \
 CUDNN \
 50 \
 dif \
 0.9 \
 VOC2012 \
 /dataset/Pascal_VOC/VOCdevkit/VOC2012/ \
 2 \
 16 \
 2.5e-4 5e-4 \
 /result/hierachy_dif \
 200 \
 21
