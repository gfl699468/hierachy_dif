#/bin/bash
python inference_msc_crf.py \
CUDNN \
50 \
dif \
0.9 \
PascalContext \ #dataset
/app/dataset/VOC2010/ \ #dataset path
2 \
3 \ #batchsize 3/3=1
2.5e-4 5e-4 \
/app/hierachy_dif/inference_result/ \
200 \
59 \ # class number
/app/hierachy_dif/valid_model_best.pth.tar \ #checkpoint file path
val
