FROM nvidia/cuda:9.0-cudnn7-devel-ubuntu16.04

SHELL ["/bin/bash","-c"]

# Install curl and sudo
RUN apt-get update && apt-get install -y \
    curl \
    ca-certificates \
    sudo \
 && rm -rf /var/lib/apt/lists/*

 # Create a working directory
RUN mkdir /app
WORKDIR /app

# Create a non-root user and switch to it
RUN adduser --disabled-password --gecos '' --shell /bin/bash user \
 && chown -R user:user /app
RUN echo "user ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/90-user
USER user

# Install Miniconda
RUN curl -o ~/miniconda.sh https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh \
 && chmod +x ~/miniconda.sh \
 && ~/miniconda.sh -b -p ~/miniconda \
 && rm ~/miniconda.sh

ENV PATH="~/miniconda/bin:${PATH}"
RUN conda install pytorch torchvision -c pytorch

# Install git
RUN function retry { sudo apt-get update && sudo apt-get install git-all -y && echo "success" || (echo "fail" && retry)}; retry

# Install Torchnet, a high-level framework for PyTorch
RUN pip install git+https://github.com/pytorch/tnt.git@master

# Install tensorboardX, Tensorboard for PyTorch
RUN pip install git+https://github.com/lanpa/tensorboard-pytorch
RUN conda install -c conda-forge tensorflow
RUN conda install tensorboard

# Install IPython
RUN conda install jupyter

# Install utiltools
RUN sudo apt-get install vim -y
RUN sudo apt-get install tmux -y

CMD ["/bin/bash"]

