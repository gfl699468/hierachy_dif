# encoding=utf8  

import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
warnings.filterwarnings("ignore", message="volatile was removed and now has no effect.")

import os
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import numpy as np
from torch.autograd import Variable, Function
import torch.backends.cudnn as cudnn
from torch.utils import data
import torchvision.models as models
from params import MyArgumentParser
import model_resnet_dif_mask as model_dif
import model_resnet_nodif as model_nodif
import basic_function as func
import CustomDataset_mseed as CustomDataset
import torchnet as tnt
import torch.multiprocessing as mp
from PIL import Image
parserWarpper = MyArgumentParser(inference=True)
parser = parserWarpper.get_parser()
args = parser.parse_args()
import time
opt_manualSeed = 1000

np.random.seed(opt_manualSeed)
torch.manual_seed(opt_manualSeed)
torch.cuda.manual_seed_all(opt_manualSeed)

#reset task affinity
os.system("taskset -p 0xff %d" % os.getpid())

#get CPU count
process_num = mp.cpu_count()-1

print("Has {} cpu core, will setup {} processes, rest CPU are preserved for GPU".format(mp.cpu_count(), process_num))

# CRF params
MAX_ITER = 10
POS_W = 3
POS_XY_STD = 1
Bi_W = 4
Bi_XY_STD = 67
Bi_RGB_STD = 3

#cudnn.enabled = True
cudnn.benchmark = True
#cudnn.deterministic = True
os.environ["CUDA_VISIBLE_DEVICES"] = args.gpus

arg_numclasses = args.numclasses

def post_process(img, pred_sg_up, gt, idx):
    import pydensecrf.utils as utils
    import pydensecrf.densecrf as dcrf
    img = img.numpy().astype(np.uint8).squeeze()
    pred_sg_up = pred_sg_up.numpy().squeeze()
    sys.stdout.flush()
    c = pred_sg_up.shape[0]
    h = pred_sg_up.shape[1]
    w = pred_sg_up.shape[2]
    img = img.transpose((1,2,0))
    U = utils.unary_from_softmax(pred_sg_up)
    U = np.ascontiguousarray(U)
    img = np.ascontiguousarray(img)

    d = dcrf.DenseCRF2D(w, h, c)
    d.setUnaryEnergy(U)
    d.addPairwiseGaussian(sxy=POS_XY_STD, compat=POS_W)
    d.addPairwiseBilateral(sxy=Bi_XY_STD, srgb=Bi_RGB_STD, rgbim=img, compat=Bi_W)

    pred_sg_up_crf = d.inference(MAX_ITER)
    pred_sg_up_crf = np.array(pred_sg_up_crf).reshape((c, h, w))
    sys.stdout.flush()
    valid_pixel = gt.ne(255).cpu().squeeze()
    pred_sg_up_crf_label = torch.from_numpy(np.ascontiguousarray(pred_sg_up_crf.argmax(0)))
    gt_numpy = gt.cpu().squeeze()
    pred, gt = pred_sg_up_crf_label[valid_pixel], gt_numpy[valid_pixel]
    return [pred, gt, idx]
    
def inference(val_loader, model, num_label=21):
    start = time.time()
    model.eval()
    pool = mp.Pool(process_num)
    confusion_meter = tnt.meter.ConfusionMeter(num_label, normalized=False)
    for i, batch in enumerate(val_loader):
        def post_callback(res):
            r1, r2, idx = res
            confusion_meter.add(r1,r2)
            #print("[{}/{}] finished".format(idx,len(val_loader)))
        img, gt = batch
        print("[{}/{}] begin inference".format(i,len(val_loader)))
        batch_size = img.size()[0]
        input_size = img.size()[2:4]
        inter_size = [64,64]

        interpo1 = nn.Upsample(size=input_size, mode='bilinear')
        interpo2 = nn.Upsample(size=inter_size, mode='bilinear')

        img075 = nn.Upsample(size=(int(input_size[0]*0.75), int(input_size[1]*0.75)), mode='bilinear')(img)
        img050 = nn.Upsample(size=(int(input_size[0]*0.50), int(input_size[1]*0.50)), mode='bilinear')(img)

        img100_v = Variable(img, volatile=True).cuda()
        img075_v = Variable(img075.data, volatile=True).cuda()
        img050_v = Variable(img050.data, volatile=True).cuda()
        gt_v = Variable(gt, volatile=True).cuda()

        if args.model_type == 'dif':
            mask100, seed100, pred100 = model(img100_v)
            mask075, seed075, pred075 = model(img075_v)
            mask050, seed050, pred050 = model(img050_v)
            pred_seed_up = interpo1(seed100)
        else:
            pred100 = model(img100_v)
            pred075 = model(img075_v)
            pred050 = model(img050_v)
        
        pred_sg_up = interpo1(torch.max(torch.stack([interpo2(pred100), interpo2(pred075), interpo2(pred050)]), dim=0)[0])
        img = (img - img.min())/(img.max()-img.min())*255
        pred_sg_up = nn.functional.softmax(Variable(pred_sg_up.data.cpu().clone(), volatile=True)).data
        #CRF
        pool.apply_async(post_process, args=(img.cpu().clone(), pred_sg_up.cpu().clone(), gt.cpu().clone(), i), callback=post_callback)
        
        #confusion_meter.add(pred, gt)
        
        #valid_pixel = gt.ne(255).cpu().squeeze()
        #pred_sg_up_label = pred_sg_up.data.cpu().squeeze().max(0)[1]
	#print(pred_sg_up_label.shape)
        #gt_numpy = gt.cpu().squeeze()
        #confusion_meter.add(pred_sg_up_label[valid_pixel], gt_numpy[valid_pixel])
    #print("begin to collect the confusion matrix...")
    #for ins in instance_list:
    #    print("processing {}".format(ins))
    #    r1, r2 = ins.get()
    #    confusion_meter.add(r1, r2)
    pool.close()
    pool.join()
    confusion_matrix = confusion_meter.value()

    inter = np.diag(confusion_matrix)
    union = confusion_matrix.sum(1).clip(min=1e-12) + confusion_matrix.sum(0).clip(min=1e-12) - inter

    mean_iou_ind = inter/union
    mean_iou_all = mean_iou_ind.mean()
    end = time.time()
    print("{}min".format((end-start)/60.))
    print(' * IOU_All {iou}'.format(iou=mean_iou_all))
    print(' * IOU_Ind {iou}'.format(iou=mean_iou_ind))

if __name__ == '__main__':

    if args.dataset == 'VOC2012':
        val_dataset=CustomDataset.ImageFolderForVOC2012(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True)
    elif args.dataset == 'PascalContext':
        val_dataset=CustomDataset.ImageFolderForPascalContext(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True)
    val_data_loader = data.DataLoader(val_dataset, num_workers=args.workers, batch_size=int(args.batchsize/3), shuffle=False)

    if '+' in args.layers:
        layers_sed = args.layers.split('+')[0]
        layers_dif = args.layers.split('+')[1]
    else:
        layers_sed = args.layers
        layers_dif = args.layers
    resnet_sed = models.__dict__['resnet' + layers_sed](pretrained=True)
    resnet_dif = models.__dict__['resnet' + layers_dif](pretrained=True)
    if args.model_type == 'dif':
        DifNet = model_dif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
        DifNet.model_sed = func.param_restore(DifNet.model_sed, resnet_sed.state_dict())
        DifNet.model_dif = func.param_restore(DifNet.model_dif, resnet_dif.state_dict())
    elif args.model_type == 'nodif':
        DifNet = model_nodif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
        DifNet = func.param_restore(DifNet, resnet_sed.state_dict())    
    else:
        print('Unknow value of model_type: {}'.format(args.model_type))
        exit()

    #load checkpoint file
    checkpoint_file = torch.load(args.checkpoint_path)
    checkpoint_file_new = {entry[7:] : checkpoint_file['state_dict'][entry].cpu() for entry in checkpoint_file['state_dict'].keys()}
    DifNet.load_state_dict(checkpoint_file_new)

    DifNet = DifNet.cuda()
    inference(val_data_loader, DifNet, arg_numclasses)

