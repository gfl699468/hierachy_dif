import os
import sys
import time
import shutil
import random
import numpy as np
import torchnet as tnt
from params import MyArgumentParser
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchvision.utils as vutils
import torchvision.models as models
import torch.backends.cudnn as cudnn
from torch.autograd import Variable, Function
from torch.utils import data
from tensorboardX import SummaryWriter
from datetime import datetime
import socket

import threading

import model_resnet_dif_mask as model_dif
import model_resnet_nodif as model_nodif
import basic_function as func
import CustomDataset_mseed as CustomDataset


parserWarpper = MyArgumentParser()
parser = parserWarpper.get_parser()
args = parser.parse_args()

opt_manualSeed = 1000#random.randint(1, 10000)  # fix seed
print("Random Seed: ", opt_manualSeed)
np.random.seed(opt_manualSeed)
torch.manual_seed(opt_manualSeed)
torch.cuda.manual_seed_all(opt_manualSeed)

#cudnn.enabled = True
cudnn.benchmark = True
#cudnn.deterministic = True
os.environ["CUDA_VISIBLE_DEVICES"] = args.gpus



arg_epochs = args.epochs
arg_numclasses = args.numclasses
arg_lr = args.lr
arg_momentum = args.momentum
arg_wdecay = args.weight_decay


def train(train_loader, model, criterion, optimizer, basic_lr, epoch, epoch_total, writer, num_label=21, iter_size =10., print_freq=100):
    lock = threading.Lock()
    model.train()
    losses = func.AverageMeter()
    iter_loss = 0
    epoch_end = time.time()
    end = time.time()
    threadlist = []
    for i, batch in enumerate(train_loader):
        img, gt = batch

        batch_size = img.size()[0]
        input_size = img.size()[2:4]
        inter_size = [41, 41]
        
        img_v = Variable(img).cuda()
        gt_v = Variable(gt).cuda()
	
        if args.model_type == 'dif':
            mask, seed, pred = model(img_v)
            pred_seed_up = F.upsample(seed, size=input_size, mode='bilinear')
            alpha = model.module.get_alpha()
            pred_sg_up = F.upsample(pred, size=input_size, mode='bilinear')
        else:
            pred = model(img_v)
            pred_sg_up = F.upsample(pred, size=input_size, mode='bilinear')

        loss_sg = criterion(pred_sg_up, gt_v.squeeze(1))
        loss = loss_sg
        losses.update(loss.data[0], img.size(0))

        current_lr = func.adjust_learning_rate(optimizer, basic_lr, epoch*len(train_loader)+i, epoch_total*len(train_loader))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        batch_time = time.time() - end
        end = time.time()
        writer.add_scalar('Loss_train', loss, epoch*len(train_loader)+i)
        writer.add_scalar('lr', current_lr, epoch*len(train_loader)+i)


        if i % print_freq == 0:
            print('Train: [{0}][{1}/{2}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   epoch, i, len(train_loader), batch_time=batch_time, loss=losses))
            if args.model_type == 'dif':
                def stream_data_to_tensorboard(img, pred_sg_up, pred_seed_up, gt, mask):
                    lock.acquire()
                    show_img = vutils.make_grid(func.BGR2RGB(img), normalize=True, scale_each=True, pad_value=0)
                    writer.add_image('img', show_img, epoch*len(train_loader)+i)
                    
                    show_mask = vutils.make_grid(mask, normalize=True)
                    writer.add_image('mask', show_mask, epoch*len(train_loader)+i)
                    
                    pred_sg_map = func.ColorMapping(func.map_decode(torch.max(pred_sg_up,1,keepdim=True)[1]))
                    show_pred_sg = vutils.make_grid(pred_sg_map, normalize=False, pad_value=1)
                    writer.add_image('sg', show_pred_sg, epoch*len(train_loader)+i)

                    pred_seed_map = func.ColorMapping(func.map_decode(torch.max(pred_seed_up,1,keepdim=True)[1]))
                    show_pred_seed = vutils.make_grid(pred_seed_map, normalize=False, pad_value=1)
                    writer.add_image('seed', show_pred_seed, epoch*len(train_loader)+i)

                    gt_map = func.ColorMapping(func.map_decode(gt))
                    show_gt = vutils.make_grid(gt_map, normalize=False, pad_value=1)
                    writer.add_image('gt', show_gt, epoch*len(train_loader)+i)
                    lock.release()
                threadlist.append(threading.Thread(target=stream_data_to_tensorboard, args=(img[1:5,::].cpu().clone(), pred_sg_up.data[1:5,::].cpu().clone(),pred_seed_up.data[1:5,::].cpu().clone(), gt[1:5,::].cpu().clone(), mask[1:5,::].data.cpu().clone())))
                threadlist[-1].start()
    print('main thread finished, waiting for IO threads...')
    for thread in threadlist:
        thread.join()

    epoch_time = time.time() - epoch_end
    print('Train time : {:.2f}min'.format(epoch_time/60))

def validate_tnt(val_loader, model, criterion, epoch, writer, num_label=21, print_freq=100):
    model.eval()
    lock = threading.Lock()
    threadlist = []
    confusion_meter = tnt.meter.ConfusionMeter(num_label, normalized=False)
    losses = func.AverageMeter()
    epoch_end = time.time()
    end = time.time()
    for i, batch in enumerate(val_loader):
        img, gt = batch

        batch_size = img.size()[0]
        input_size = img.size()[2:4]

        img_v = Variable(img, volatile=True).cuda()
        gt_v = Variable(gt, volatile=True).cuda()
        if args.model_type == 'dif':
            mask, seed, pred = model(img_v)
            pred_seed_up = F.upsample(seed, size=input_size, mode='bilinear')
            alpha = model.module.get_alpha()
        else:
            pred = model(img_v)

        pred_sg_up = F.upsample(pred, size=input_size, mode='bilinear')

        loss = criterion(pred_sg_up, gt_v.squeeze(1))
        
        valid_pixel = gt.ne(255)
        pred_sg_up_label = pred_sg_up.max(1)[1]
        
        confusion_meter.add(pred_sg_up_label.data.cpu()[valid_pixel], gt.cpu()[valid_pixel])
        losses.update(loss.data[0], img.size(0))
        batch_time = time.time() - end
        end = time.time()

        if i % print_freq == 0:
            print('Valid: [{0}/{1}]\t'
                  'Time {batch_time:.3f}\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})'.format(
                   i, len(val_loader), batch_time=batch_time, loss=losses))
            if args.model_type == 'dif':
                print(alpha)
                def stream_data_to_tensorboard(img, pred_sg_up, pred_seed_up, gt):
                    lock.acquire()
                    show_img = vutils.make_grid(func.BGR2RGB(img), normalize=True, scale_each=True, pad_value=0)
                    writer.add_image('img_valid', show_img, epoch*len(val_loader)+i)

                    pred_sg_map = func.ColorMapping(func.map_decode(torch.max(pred_sg_up,1,keepdim=True)[1]))
                    show_pred_sg = vutils.make_grid(pred_sg_map, normalize=False, pad_value=1)
                    writer.add_image('sg_valid', show_pred_sg, epoch*len(val_loader)+i)
                
                    gt_map = func.ColorMapping(func.map_decode(gt))
                    show_gt = vutils.make_grid(gt_map, normalize=False, pad_value=1)
                    writer.add_image('gt_valid', show_gt, epoch*len(val_loader)+i)
                    lock.release()
                threadlist.append(threading.Thread(target=stream_data_to_tensorboard, args=(img[1:5,::].cpu().clone(), pred_sg_up.data[1:5,::].cpu().clone(),pred_seed_up.data[1:5,::].cpu().clone(), gt[1:5,::].cpu().clone())))
                threadlist[-1].start()
    print('main thread finished, waiting for IO threads...')
    for thread in threadlist:
        thread.join()
    confusion_matrix = confusion_meter.value()

    inter = np.diag(confusion_matrix)
    union = confusion_matrix.sum(1).clip(min=1e-12) + confusion_matrix.sum(0).clip(min=1e-12) - inter

    mean_iou_ind = inter/union
    mean_iou_all = mean_iou_ind.mean()

    print(' * IOU_All {iou}'.format(iou=mean_iou_all))
    print(' * IOU_Ind {iou}'.format(iou=mean_iou_ind))

    if writer!=None:
        writer.add_scalar('Loss_valid', losses.avg, epoch)
        writer.add_scalar('IOU_valid', mean_iou_all, epoch)
    epoch_time = time.time() - epoch_end
    print('Train time : {:.2f}min'.format(epoch_time/60))
    return mean_iou_all, mean_iou_ind


#def init_fn(worker_id):
#    torch.manual_seed(opt_manualSeed + worker_id)


if args.dataset == 'VOC2012':
    train_dataset=CustomDataset.ImageFolderForVOC2012(args.dataset_path, 'train.txt', 321, flip=True, scale=True, crop=True, resize=False, pad=False)
    val_dataset=CustomDataset.ImageFolderForVOC2012(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True)
elif args.dataset == 'PascalContext':
    train_dataset=CustomDataset.ImageFolderForPascalContext(args.dataset_path, 'train.txt', 321, flip=True, scale=True, crop=True, resize=False, pad=False)
    val_dataset=CustomDataset.ImageFolderForPascalContext(args.dataset_path, 'val.txt', 505, flip=False, scale=False, crop=False, resize=False, pad=True)

train_data_loader = data.DataLoader(train_dataset, num_workers=args.workers, batch_size=args.batchsize, shuffle=True)
val_data_loader = data.DataLoader(val_dataset, num_workers=args.workers, batch_size=int(args.batchsize/2), shuffle=False)


#513

if '+' in args.layers:
    layers_sed = args.layers.split('+')[0]
    layers_dif = args.layers.split('+')[1]
else:
    layers_sed = args.layers
    layers_dif = args.layers
resnet_sed = models.__dict__['resnet' + layers_sed](pretrained=True)
resnet_dif = models.__dict__['resnet' + layers_dif](pretrained=True)
if args.model_type == 'dif':
    DifNet = model_dif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
    DifNet.model_sed = func.param_restore(DifNet.model_sed, resnet_sed.state_dict())
    DifNet.model_dif = func.param_restore(DifNet.model_dif, resnet_dif.state_dict())
elif args.model_type == 'nodif':
    DifNet = model_nodif.Res_Deeplab(num_classes=arg_numclasses, layers=args.layers)
    DifNet = func.param_restore(DifNet, resnet_sed.state_dict())    
else:
	print('Unknow value of model_type: {}'.format(args.model_type))
	exit()
DifNet = torch.nn.DataParallel(DifNet)
DifNet = DifNet.cuda()


optimizer = optim.SGD(DifNet.parameters(),lr=arg_lr, momentum=arg_momentum, weight_decay=arg_wdecay)

criterion = func.SegLoss(255)

result_path = parserWarpper.result_path(vars(args).copy())

date = datetime.now().strftime('%b%d_%H-%M-%S')+'_'+socket.gethostname()

writer = SummaryWriter(args.result_path + '/runs/' + result_path + date)
best_iou = 0
best_iou_ind = 0
best_alpha = 0

checkpoint_path = args.result_path + '/checkpoint/' + result_path + date
directory = checkpoint_path
if not os.path.exists(directory):
    os.makedirs(directory)

for epoch in range(arg_epochs):
    # train for one epoch
    train(train_data_loader, DifNet, criterion, optimizer, arg_lr, epoch, arg_epochs, writer, num_label=arg_numclasses)

    # evaluate on validation set
    iou_all, iou_ind = validate_tnt(val_data_loader, DifNet, criterion, epoch, writer, num_label=arg_numclasses)

    # remember best prec@1 and save checkpoint
    is_best = iou_all > best_iou
    best_iou = iou_all if is_best else best_iou
    best_iou_ind = iou_ind if is_best else best_iou_ind
    if args.model_type == 'dif':
        best_alpha = DifNet.module.get_alpha() if is_best else best_alpha
        func.save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': DifNet.state_dict(),
            'best_iou': (best_iou, best_iou_ind),
            'best_alpha': best_alpha,
            'optimizer': optimizer.state_dict(),
        }, is_best, tag='valid', check_dir=checkpoint_path)
    else:
        func.save_checkpoint({
            'epoch': epoch + 1,
            'state_dict': DifNet.state_dict(),
            'best_iou': (best_iou, best_iou_ind),
            'optimizer': optimizer.state_dict(),
        }, is_best, tag='valid', check_dir=checkpoint_path)
