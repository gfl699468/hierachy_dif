import argparse


class MyArgumentParser():
    def __init__(self, inference=False):
        self.ignored_args = ['result_path', 'dataset_path']
        self.parser = argparse.ArgumentParser(description='PyTorch Hierachy_dif Training')
        self.parser.add_argument('gpus', metavar='GPUS', help='GPU ID')
        self.parser.add_argument('backend', metavar='BACKEND', help='the backend of training: GPU|CUDNN')
        self.parser.add_argument('layers', metavar='LAYERS', help='the layer number of resnet: 18, 34, 50, 101, 152')
        self.parser.add_argument('model_type', metavar='MODEL_TYPE', help='model type: dif, nodif')
        self.parser.add_argument('momentum', default=0.9, type=float, metavar='MOMENTUM', help='the momentum of SGD learning algorithm')
        self.parser.add_argument('dataset', metavar='DATASET', help='dataset: VOC2012|PascalContext')
        self.parser.add_argument('dataset_path', metavar='DATASET_PATH', help='path to the dataset(multiple paths are concated by "+")')
        self.parser.add_argument('workers', default=5, type=int, metavar='WORKERS', help='number of dataload worker')
        self.parser.add_argument('batchsize', type=int, metavar='BATCH_SIZE', help='batchsize')
        self.parser.add_argument('lr', default=2.5e-4, type=float, metavar='LEARNING_RATE', help='learning rate')
        self.parser.add_argument('weight_decay', default=0.05, type=float, metavar='WEIGHT_DECAY', help='weight decay')
        self.parser.add_argument('result_path', metavar='RESULT_PATH', help='path to the log and checkpoint')
        self.parser.add_argument('--start_epoch', default=0, dest='start_epoch', type=int, metavar='S', help='(manually) start epoch')
        self.parser.add_argument('epochs', default=50, type=int, metavar='EPOCH',help='number of total epochs to run')
        self.parser.add_argument('numclasses', type=int, metavar='NUMCLASSES', help='number of classes')
        if inference:
            self.parser.add_argument('checkpoint_path', metavar='CHECKPOINT_PATH', help='path to the checkpoint file')
            self.parser.add_argument('inference_dataset', default='val', metavar='INFERENCE_DATASET', help='the dataset you want to inference: val|test')
            self.parser.add_argument('--internal_data', dest='internal_data', action='store_true', help='save the internal data')
    def get_parser(self):
        return self.parser
    def result_path(self, args_):
        path = ''
        for name in self.ignored_args:
            val = args_.pop(name, None)
        for name in sorted(args_):
            path += '{}={}\\'.format(name, args_[name])
        return path
